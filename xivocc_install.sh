#!/bin/bash
set -e

mirror_xivo="http://mirror.xivo.solutions"
update='apt-get update'
install='apt-get install --assume-yes'
download='apt-get install --assume-yes --download-only'
dcomp='/usr/bin/xivocc-dcomp'

distribution='xivo-callisto'
dev_lts='deneb'
repo='debian'
debian_name='stretch'
debian_version='9'


get_system_architecture() {
    architecture=$(uname -m)
}

check_system() {
    local version_file='/etc/debian_version'
    if [ ! -f $version_file ]; then
        echo "You must install XiVO CC on a Debian $debian_version (\"$debian_name\") system"
        echo "You do not seem to be on a Debian system"
        exit 1
    else
        version=$(cut -d '.' -f 1 "$version_file")
    fi

    if [ $version != $debian_version ]; then
        echo "You must install XiVO CC on a Debian $debian_version (\"$debian_name\") system"
        echo "You are currently on a Debian $version system"
        exit 1
    fi
}

add_dependencies() {
	$update
    # This is needed by apt-key in Debian 9
	$install ca-certificates \
		dirmngr \
		ntp
}

add_xivo_key() {
    wget $mirror_xivo/xivo_current.key -O - | apt-key add -
}

add_docker_key() {
    echo "Adding Docker GPG key..."
    apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 9DC858229FC7DD38854AE2D88D81803C0EBFCD88
}

add_mirror() {
    echo "Add mirrors informations"
    local mirror="deb $mirror_xivo/$repo $distribution main"
    apt_dir="/etc/apt"
    sources_list_dir="$apt_dir/sources.list.d"
    if ! grep -qr "$mirror" "$apt_dir"; then
        echo "$mirror" > $sources_list_dir/tmp-pf.sources.list
    fi
    add_xivo_key
    add_docker_key

    export DEBIAN_FRONTEND=noninteractive
    $update
    $install xivo-dist
    xivo-dist "$distribution"

    rm -f "$sources_list_dir/tmp-pf.sources.list"
    $update
}

install_xivocc () {
    # Install docker stuff
    ${install} xivo-docker
    ${install} docker-ce

    ${download} xivocc-installer
    ${install} xivocc-installer

    if [ -f "${dcomp}" ]; then
         ${dcomp} pull
    fi

}

check_system_is_64bit() {
    if [ "$architecture" != "x86_64" ]; then
        echo $not_installable_message
        exit 1
    fi
}

check_archive_prefix() {
    if [ "${1::5}" = "xivo-" ]; then
        echo 'Archive version must be typed without the prefix "xivo-"'
        exit 1
    fi
}


usage() {
    cat << EOF
    This script is used to install XiVO CC

    usage : $(basename $0) {-d|-r|-a version}
        whitout arg : install production version
        -d          : install development version
        -a version  : install archived version (2018.16 or later)

EOF
    exit 1
}

get_system_architecture
check_system_is_64bit

while getopts ':dra:' opt; do
    case ${opt} in
        d)
            distribution="xivo-$dev_lts-dev"
            ;;
        a)
            check_archive_prefix $OPTARG
            distribution="xivo-$OPTARG"
            repo='archive'

            if ! [ "$OPTARG" \> "2018.15" ]; then
                # 2018.16 and earlier don't have mds available
                echo "This script only supports installing XiVO 2018.16 or later."
                exit 1
            fi
            ;;
        *)
            usage
            ;;
    esac
done

check_system
add_dependencies
add_mirror
install_xivocc
