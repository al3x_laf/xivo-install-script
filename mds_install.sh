#!/bin/bash
set -e

mirror_xivo="http://mirror.xivo.solutions"
update='apt-get update'
install='apt-get install --assume-yes'
download='apt-get install --assume-yes --download-only'
dcomp='/usr/bin/xivo-dcomp'
custom_env='/etc/docker/mds/custom.env'

distribution='xivo-callisto'
dev_lts='deneb'
repo='debian'
debian_name='stretch'
debian_version='9'
not_installable_message='This version of XiVO is not installable on 32-bit systems.'


get_system_architecture() {
    architecture=$(uname -m)
}

check_system() {
    local version_file='/etc/debian_version'
    if [ ! -f $version_file ]; then
        echo "You must install XiVO on a Debian $debian_version (\"$debian_name\") system"
        echo "You do not seem to be on a Debian system"
        exit 1
    else
        version=$(cut -d '.' -f 1 "$version_file")
    fi

    if [ $version != $debian_version ]; then
        echo "You must install XiVO on a Debian $debian_version (\"$debian_name\") system"
        echo "You are currently on a Debian $version system"
        exit 1
    fi
}

add_dependencies() {
    $update
    # This is needed by apt-key in Debian 9
    $install dirmngr
}

add_xivo_key() {
    wget $mirror_xivo/xivo_current.key -O - | apt-key add -
}

add_docker_key() {
    echo "Adding Docker GPG key..."
    apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 9DC858229FC7DD38854AE2D88D81803C0EBFCD88
}

add_xivo_apt_conf() {
    echo "Installing APT configuration file: /etc/apt/apt.conf.d/90xivo"

	cat > /etc/apt/apt.conf.d/90xivo <<-EOF
	Aptitude::Recommends-Important "false";
	APT::Install-Recommends "false";
	APT::Install-Suggests "false";
	EOF
}

add_mirror() {
    echo "Add mirrors informations"
    local mirror="deb $mirror_xivo/$repo $distribution main"
    apt_dir="/etc/apt"
    sources_list_dir="$apt_dir/sources.list.d"
    if ! grep -qr "$mirror" "$apt_dir"; then
        echo "$mirror" > $sources_list_dir/tmp-pf.sources.list
    fi
    add_xivo_key
    add_docker_key

    export DEBIAN_FRONTEND=noninteractive
    $update
    $install xivo-dist
    xivo-dist "$distribution"

    rm -f "$sources_list_dir/tmp-pf.sources.list"
    $update
}

add_pgdg_mirror() {
    $install wget ca-certificates lsb-release
    wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -
    sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
    $update
}

get_xivo_host () {
    if [ -f ${custom_env} ]; then
        XIVO_HOST=$(grep XIVO_HOST ${custom_env} | awk -F "=" '/1/ {print $2}')
    fi
    if [ -z ${XIVO_HOST} ]; then
        XIVO_HOST=$(whiptail --inputbox "Enter the XiVO MAIN Server(mds0) IP Address: " 8 50 3>&1 1>&2 2>&3)
    fi
}

install_dahdi_modules() {
    flavour=$(echo $(uname -r) | cut -d\- -f3)
    kernel_release=$(ls /lib/modules/ | grep ${flavour})
    for kr in ${kernel_release}; do
        ${download} dahdi-linux-modules-${kr}
        ${install} dahdi-linux-modules-${kr}
    done
}

install_mds () {
    # Only install dahdi-linux-modules if the package dahdi-linux-dkms was not found
    dahdi_dkms_exists=$(apt-cache pkgnames | grep -q "dahdi-linux-dkms"; echo $?)
    if [ $dahdi_dkms_exists -eq 0 ]; then
      echo "DAHDI: dahdi-linux-dkms package found in repository"
    else
      echo "DAHDI: dahdi-linux-dkms package not found in repository"
      install_dahdi_modules
    fi

    # Install docker stuff
    ${install} xivo-docker
    ${install} docker-ce

    # Install asterisk
    ${download} asterisk xivo-res-freeze-check asterisk-dbg asterisk-sounds-main asterisk-sounds-wav-en-us asterisk-sounds-wav-fr-fr xivo-sounds-en-us xivo-sounds-fr-fr
    ${install} asterisk xivo-res-freeze-check asterisk-dbg asterisk-sounds-main asterisk-sounds-wav-en-us asterisk-sounds-wav-fr-fr xivo-sounds-en-us xivo-sounds-fr-fr
    # Install asterisk default moh
    ${install} asterisk-moh-opsound-g722 asterisk-moh-opsound-gsm asterisk-moh-opsound-wav

    # Currently best way to install and initialize Database
    # TODO: need of some improvement there
    ${install} xivo-manage-db-mds

    ${install} xivo-auth-keys       # Needed to have xivo-update-keys
    ${install} xivo-config          # Needed for the dialplan

    ${install} xivo-confgend-client # Needed by asterisk to connect to confgend

    # For configuration reload
    ${install} xivo-bus xivo-sysconfd

    ${download} xivo-mds-installer
    ${install} xivo-mds-installer

    # Monitoring
    ${install} xivo-monitoring-mds

    ${install} xivo-service

    configure_mds

    /usr/bin/xivo-update-keys
    /usr/bin/xivo-configure-uuid

    if [ -f "${dcomp}" ]; then
         ${dcomp} pull
    fi

    xivo-service restart all
}

configure_mds () {
    echo "Configuring XiVO"

    get_xivo_host
    echo "XIVO HOST IS $XIVO_HOST"
    echo "db_uri: postgresql://asterisk:proformatique@$XIVO_HOST/asterisk" > /etc/xivo-dao/conf.d/010-mds_db_uri.yml

    #used by xivo-service and start only service used by mds
    touch /var/lib/xivo/mds_enabled
}


check_system_is_64bit() {
    if [ "$architecture" != "x86_64" ]; then
        echo $not_installable_message
        exit 1
    fi
}

check_archive_prefix() {
    if [ "${1::5}" = "xivo-" ]; then
        echo 'Archive version must be typed without the prefix "xivo-"'
        exit 1
    fi
}

usage() {
    cat << EOF
    This script is used to install XiVO MDS

    usage : $(basename $0) {-d|-r|-a version}
        whitout arg : install production version
        -d          : install development version
        -a version  : install archived version (2018.11 or later)

EOF
    exit 1
}

get_system_architecture
check_system_is_64bit

while getopts ':dra:' opt; do
    case ${opt} in
        d)
            distribution="xivo-$dev_lts-dev"
            ;;
        a)
            check_archive_prefix $OPTARG
            distribution="xivo-$OPTARG"
            repo='archive'

            if ! [ "$OPTARG" \> "2018.11" ]; then
                # 2018.11 and earlier don't have mds available
                echo "This script only supports installing XiVO 2018.12 or later."
                exit 1
            fi
            ;;
        *)
            usage
            ;;
    esac
done

check_system
add_xivo_apt_conf
add_dependencies
add_mirror
add_pgdg_mirror
install_mds
