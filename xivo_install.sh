#!/bin/bash
set -e

mirror_xivo="http://mirror.xivo.solutions"
update='apt-get update'
install='apt-get install --assume-yes'
download='apt-get install --assume-yes --download-only'
dcomp='/usr/bin/xivo-dcomp'
distribution='xivo-callisto'
dev_lts='deneb'
repo='debian'
debian_name='stretch'
debian_version='9'
not_installable_message='This version of XiVO is not installable on 32-bit systems.'


get_system_architecture() {
    architecture=$(uname -m)
}

check_system() {
    local version_file='/etc/debian_version'
    if [ ! -f $version_file ]; then
        echo "You must install XiVO on a Debian $debian_version (\"$debian_name\") system"
        echo "You do not seem to be on a Debian system"
        exit 1
    else
        version=$(cut -d '.' -f 1 "$version_file")
    fi

    if [ $version != $debian_version ]; then
        echo "You must install XiVO on a Debian $debian_version (\"$debian_name\") system"
        echo "You are currently on a Debian $version system"
        exit 1
    fi
}

add_dependencies() {
    $update
    # This is needed by apt-key
    $install dirmngr
}

add_xivo_key() {
    wget $mirror_xivo/xivo_current.key -O - | apt-key add -
}

add_docker_key() {
    echo "Adding Docker GPG key..."
    apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 9DC858229FC7DD38854AE2D88D81803C0EBFCD88
}

add_docker-engine_key() {
    echo "Adding Docker Engine GPG key..."
    apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
}


add_xivo_apt_conf() {
    echo "Installing APT configuration file: /etc/apt/apt.conf.d/90xivo"

	cat > /etc/apt/apt.conf.d/90xivo <<-EOF
	Aptitude::Recommends-Important "false";
	APT::Install-Recommends "false";
	APT::Install-Suggests "false";
	EOF
}

add_mirror() {
    echo "Add mirrors informations"
    local mirror="deb $mirror_xivo/$repo $distribution main"
    apt_dir="/etc/apt"
    sources_list_dir="$apt_dir/sources.list.d"
    if ! grep -qr "$mirror" "$apt_dir"; then
        echo "$mirror" > $sources_list_dir/tmp-pf.sources.list
    fi
    add_xivo_key
    add_docker_key

    export DEBIAN_FRONTEND=noninteractive
    $update
    $install xivo-dist
    xivo-dist "$distribution"

    rm -f "$sources_list_dir/tmp-pf.sources.list"
    $update
}

add_pgdg_mirror() {
    $install wget ca-certificates lsb-release
    wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -
    sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
    $update
}

install_dahdi_modules() {
    flavour=$(echo $(uname -r) | cut -d\- -f3)
    kernel_release=$(ls /lib/modules/ | grep ${flavour})
    for kr in ${kernel_release}; do
        ${download} dahdi-linux-modules-${kr}
        ${install} dahdi-linux-modules-${kr}
    done
}

install_xivo () {
    wget -q -O - $mirror_xivo/d-i/$debian_name/pkg.cfg | debconf-set-selections

    kernel_release=$(uname -r)
    ${install} --purge postfix

    # Only install dahdi-linux-modules if the package dahdi-linux-dkms was not found
    dahdi_dkms_exists=$(apt-cache pkgnames | grep -q "dahdi-linux-dkms"; echo $?)
    if [ $dahdi_dkms_exists -eq 0 ]; then
      echo "DAHDI: dahdi-linux-dkms package found in repository"
    else
      echo "DAHDI: dahdi-linux-dkms package not found in repository"
      install_dahdi_modules
    fi

    ${download} xivo
    ${install} xivo

    # Install asterisk default moh
    ${install} asterisk-moh-opsound-g722 asterisk-moh-opsound-gsm asterisk-moh-opsound-wav

    if [ -f "${dcomp}" ]; then
        ${dcomp} pull
    fi
    xivo-service restart all

    if [ $? -eq 0 ]; then
        echo 'You must now finish the installation'
        xivo_ip=$(ip a s eth0 | grep -E 'inet.*eth0' | awk '{print $2}' | cut -d '/' -f 1 )
        echo "open http://$xivo_ip to configure XiVO"
    fi
}

check_distribution_is_32bit() {
    if [ "$distribution" = "xivo-polaris" ] || [ "$distribution" = "xivo-five" ]; then
        return 0
    else
        return 1
    fi
}

propose_polaris_installation() {
    echo $not_installable_message
    read -p 'Would you like to install XiVO Polaris [Y/n]? ' answer
    answer="${answer:-Y}"
    if [ "$answer" != 'y' -a "$answer" != 'Y' ]; then
        exit 0
    fi
    distribution="xivo-polaris"
}

check_system_is_64bit() {
    if [ "$architecture" != "x86_64" ]; then
        echo $not_installable_message
        exit 1
    fi
}

check_archive_prefix() {
    if [ "${1::5}" = "xivo-" ]; then
        echo 'Archive version must be typed without the prefix "xivo-"'
        exit 1
    fi
}

postinst_actions() {
    # On Debian 9 Spawn-FCGI|needs to be started at the end of installation
    if [ "$debian_version" \> "7" ]; then
        systemctl start spawn-fcgi
    fi
}

usage() {
    cat << EOF
    This script is used to install XiVO

    usage : $(basename $0) {-d|-r|-a version}
        whitout arg : install production version
        -d          : install development version
        -a version  : install archived version (14.18 or later)

EOF
    exit 1
}

get_system_architecture

if [ -z $1 ] && [ "$architecture" != "x86_64" ]; then
    if ! check_distribution_is_32bit; then
        propose_polaris_installation
    fi
else
    if [[ $# -eq 0 ]]; then
        add_pgdg_mirror
    fi
    while getopts ':dra:' opt; do
        case ${opt} in
            d)
                check_system_is_64bit
                add_pgdg_mirror
                distribution="xivo-$dev_lts-dev"
                ;;
            r)
                echo "xivo-rc distribution does not exist anymore"
                echo "use option -a VERSION (e.g. -a 2018.14.00) to install a RC"
                exit 1
                ;;
            a)
                check_archive_prefix $OPTARG
                distribution="xivo-$OPTARG"
                repo='archive'

                if [ "$OPTARG" \> "2018" ]; then
                    check_system_is_64bit
                fi
                if [ "${OPTARG::7}" = "2018.02" ]; then
                    add_docker-engine_key
                fi

                if [ "$OPTARG" \> "2018.13" ]; then
                    debian_version='9'
                    debian_name='stretch'
                    add_pgdg_mirror
                elif [ "$OPTARG" \> "15.19" ]; then
                    debian_version='8'
                    debian_name='jessie'
                elif [ "$OPTARG" \> "14.17" ]; then
                    debian_version='7'
                    debian_name='wheezy'
                else
                    # 14.17 and earlier don't have xivo-dist available
                    echo "This script only supports installing XiVO 14.18 or later."
                    exit 1
                fi
                ;;
            *)
                usage
                ;;
        esac
    done
fi

check_system
add_xivo_apt_conf
add_dependencies
add_mirror
install_xivo
postinst_actions
